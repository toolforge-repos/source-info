use actix_web::{get, App, HttpResponse, HttpServer, HttpRequest};
use serde_json::{json, Value};
use serde::{Deserialize};
use url::{Url};
use qstring::QString;
use std::fs;

#[derive(Deserialize, Debug)]
struct Source {
    source: String,
    domains: Box<[String]>,
    status: SourceType
}

#[derive(Deserialize, Debug)]
enum SourceType {
    GenerallyReliable,
    NoConsensus,
    GenerallyUnreliable,
    Deprecated,
}

impl SourceType {
    pub fn as_str(&self) -> &'static str {
        match self {
            SourceType::GenerallyReliable => "generallyreliable",
            SourceType::NoConsensus => "noconsensus",
            SourceType::GenerallyUnreliable => "generallyunreliable",
            SourceType::Deprecated => "deprecated",
        }
    }
}

#[get("/")]
async fn index_get() -> HttpResponse {
    let data = json!({
        "name": "source-info",
        "version": env!("CARGO_PKG_VERSION"),
    });

    HttpResponse::Ok().json(data)
}

#[get("/info")]
async fn info(req: HttpRequest) -> HttpResponse {
    let domain = Url::parse(QString::from(req.query_string()).get("domain").unwrap()).unwrap();
    let source = domain.host_str().unwrap();

    let sources = fs::read_to_string("./src/data/sources.json").expect("Unable to parse");
    let res: Vec<Value> = serde_json::from_str(&sources).expect("Unable to parse");

    let mut name = format!("Unknown");
    let mut status = SourceType::Deprecated;
    for item in res {
        let i: Source = serde_json::from_value(item).unwrap();
        if i.domains.contains(&source.to_owned()) {
            name = i.source;
        }
        status = i.status;
    }

    let data = json!({
        "name": name,
        "domain": source,
        "status": status.as_str()
    });

    HttpResponse::Ok().json(data)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(info)
            .service(index_get)
    })
    .bind(("0.0.0.0", 8000))?
    .run()
    .await
}